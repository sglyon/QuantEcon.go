package quantecon

import (
	"errors"
	"math"
)

func myLGamma(x float64) float64 {
	out, _ := math.Lgamma(x)
	return out
}

// Qnwgamma constructs `n` quadrature nodes and weights for a random variable
// distributed Gamma(a, b), where `a` is the shape parameter and `b` is the
// scale parameter (e.g. `b` is one over the rate)
func Qnwgamma(n int, a float64, b float64) ([]float64, []float64, error) {
	if a < 0 {
		return []float64{}, []float64{}, errors.New("shape parameter must be positive")
	}
	if b < 0 {
		return []float64{}, []float64{}, errors.New("scale paramter must be positive")
	}

	a--
	maxit := 25
	fn := float64(n)
	factor := -math.Exp(myLGamma(a+fn) - myLGamma(fn) - myLGamma(a+1.0))
	nodes := make([]float64, n)
	weights := make([]float64, n)

	var z, pp, p1, p2, p3, fj, z1 float64
	var j int
	for i := 0; i < n; i++ {
		if i == 0 {
			z = (1 + a) * (3 + 0.92*a) / (1 + 2.4*fn + 1.8*a)
		} else if i == 1 {
			z = z + (15+6.25*a)/(1+0.9*a+2.5*fn)
		} else {
			j = i - 1
			fj = float64(j)
			z = z + ((1+2.55*fj)/(1.9*fj)+1.26*fj*a/(1+3.5*fj))*(z-nodes[j-1])/(1+0.3*a)
		}

		z1 = -10000.0
		its := 0
		for math.Abs(z-z1) > 1e-10 && its < maxit {
			p1 = 1.0
			p2 = 0.0
			for j := 1; j < n+1; j++ {
				fj = float64(j)
				p3 = p2
				p2 = p1
				p1 = ((2*fj-1+a-z)*p2 - (fj-1+a)*p3) / fj
			}

			pp = (fn*p1 - (fn+a)*p2) / z
			z1 = z
			z = z1 - p1/pp
			its++
		}

		if its >= maxit || math.IsNaN(z) {
			return []float64{}, []float64{}, errors.New("Failure to converge in qnwgamma")
		}

		nodes[i] = z
		weights[i] = factor / (pp * fn * p2)

	}

	for i := 0; i < n; i++ {
		nodes[i] *= b

	}
	return nodes, weights, nil
}

//     z = (1+a)*(3 + 0.92a)/(1 + 2.4n + 1.8a)
//
//     for i=1:n
//         # get starting values
//         if i==1
//             z = (1+a)*(3 + 0.92a)/(1 + 2.4n + 1.8a)
//         elseif i==2
//             z += (15 + 6.25a)./(1 + 0.9a + 2.5n)
//         else
//             j = i-2
//             z += ((1+2.55j)./(1.9j) + 1.26j*a./(1+3.5j)) * (z-nodes[j])./(1+0.3a)
//         end
//
//         # rootfinding iterations
//         it = 0
//         pp = 0.0
//         p2 = 0.0
//         for it = 1:maxit
//             p1 = 1.0
//             p2 = 0.0
//             for j=1:n
//                 # Recurrance relation for Laguerre polynomials
//                 p3 = p2
//                 p2 = p1
//                 p1 = ((2j - 1 + a - z) * p2 - (j - 1 + a) * p3) ./ j
//             end
//             pp = (n*p1-(n+a)*p2)./z
//             z1 = z
//             z = z1-p1 ./ pp
//             if abs(z - z1) < 3e-14
//                 break
//             end
//         end
//
//         if it >= maxit
//             error("failure to converge.")
//         end
//
//         nodes[i] = z
//         weights[i] = fact / (pp * n * p2)
//     end
//
//     return nodes .* b, weights
// end
