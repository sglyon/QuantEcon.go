package quantecon

import (
	"math"
	"math/rand"
	"testing"

	"gonum.org/v1/gonum/mat"

	"github.com/gonum/floats"
)

func testLinInterpEval(t *testing.T, breaks, vals []float64, truth func(float64) float64) {
	li, err := NewLinInterp(breaks, vals)
	if err != nil {
		t.Errorf("Failed creating a LinInterp with proper data")
	}

	// On grid is exact
	for ix := 0; ix < len(vals); ix++ {
		if math.Abs(li.Eval(breaks[ix])-vals[ix]) > 1e-15 {
			t.Errorf(
				"LinInterp Eval should be exact on gridpoints. Not for ix =%v", ix,
			)
		}
	}

	// Off grid is close
	newPoints := make([]float64, 150)
	floats.Span(newPoints, breaks[0], breaks[li.n-1])
	for ix := 0; ix < len(newPoints); ix++ {
		val := truth(newPoints[ix])
		if math.Abs(li.Eval(newPoints[ix])-val) > 1e-2 {
			t.Error(
				"LinInterp.Eval should be close on gridpoints. Expected", val,
				"Found", li.Eval(newPoints[ix]),
				"At point", newPoints[ix],
			)
		}
	}
	// Below grid gives first val
	for _, x := range []float64{-100000.0, -4, -3 - 1e-12} {
		if math.Abs(li.Eval(x)-li.vals[0]) > 1e-15 {
			t.Error(
				"LinInterp.Eval should equal lower bound below breaks. ",
				"Input", x,
				"Wanted", li.vals[0],
				"Found", li.Eval(x),
			)
		}
	}

	// Above grid gives first val
	for _, x := range []float64{100000.0, 4, 3 + 1e-15} {
		if math.Abs(li.Eval(x)-li.vals[li.n-1]) > 1e-15 {
			t.Error(
				"LinInterp.Eval should equal upper bound above breaks. ",
				"Input", x,
				"Wanted", li.vals[li.n-1],
				"Found", li.Eval(x),
			)
		}
	}
}

func TestLinInterp(t *testing.T) {
	// Uniform interpolation
	breaks := make([]float64, 100)
	floats.Span(breaks, -3, 3)

	vals := make([]float64, 100)
	for ix := 0; ix < len(vals); ix++ {
		vals[ix] = math.Exp(breaks[ix])
	}

	testLinInterpEval(t, breaks, vals, math.Exp)

	// Non-uniform interpolation
	breaks2 := make([]float64, 40)
	vals2 := make([]float64, 40)
	breaks2[0] = 0.01
	vals2[0] = math.Sin(breaks2[0])
	for ix := 1; ix < len(breaks2); ix++ {
		breaks2[ix] = breaks2[ix-1] + rand.Float64()*0.1
		vals2[ix] = math.Sin(breaks2[ix])
	}

	testLinInterpEval(t, breaks2, vals2, math.Sin)

	// make sure we get errors when we expect them
	_, err := NewLinInterp(breaks, vals2)
	if err == nil {
		t.Error("Expected NewLinInterp to fail because breaks and vals are unequal length")
	}

	_, err = NewLinInterp([]float64{0.1, 0.2, 0.15, 0.3}, []float64{0.1, 0.2, 0.3, 0.4})
	if err == nil {
		t.Error("Expected NewLinInterp to fail because breaks is unsorted")
	}

}

func createInterp(N int, truth func(float64) float64, lb, ub float64) *LinInterp {
	breaks := make([]float64, N)
	vals := make([]float64, N)
	floats.Span(breaks, lb, ub)
	for ix := 0; ix < N; ix++ {
		vals[ix] = truth(breaks[ix])
	}
	li, err := NewLinInterp(breaks, vals)
	if err != nil {
		panic("Somethow failed to make a LinInterp...")
	}
	return li
}

func benchmarkLinInterpEval(li *LinInterp, at float64, b *testing.B) {
	for ix := 0; ix < b.N; ix++ {
		li.Eval(at)
	}
}

func BenchmarkLinInterpEvalCos5(b *testing.B) {
	benchmarkLinInterpEval(createInterp(5, math.Cos, -1, 1), 0.1, b)
}
func BenchmarkLinInterpEvalCos10(b *testing.B) {
	benchmarkLinInterpEval(createInterp(10, math.Cos, -1, 1), 0.1, b)
}
func BenchmarkLinInterpEvalCos25(b *testing.B) {
	benchmarkLinInterpEval(createInterp(25, math.Cos, -1, 1), 0.1, b)
}
func BenchmarkLinInterpEvalCos50(b *testing.B) {
	benchmarkLinInterpEval(createInterp(50, math.Cos, -1, 1), 0.1, b)
}
func BenchmarkLinInterpEvalCos125(b *testing.B) {
	benchmarkLinInterpEval(createInterp(125, math.Cos, -1, 1), 0.1, b)
}
func BenchmarkLinInterpEvalCos300(b *testing.B) {
	benchmarkLinInterpEval(createInterp(300, math.Cos, -1, 1), 0.1, b)
}

func testLinInterpManyEval(t *testing.T, breaks []float64, vals *mat.Dense, truths []func(float64) float64) {
	li, err := NewLinInterpMany(breaks, vals)
	if err != nil {
		t.Errorf("Failed creating a LinInterpMany with proper data")
	}

	// On grid is exact
	want := make([]float64, li.ncol)
	for ix := 0; ix < len(breaks); ix++ {
		have := li.Eval(breaks[ix])
		mat.Row(want, ix, li.vals)
		for col := 0; col < li.ncol; col++ {
			if math.Abs(have[col]-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany Eval should be exact on gridpoints. Not for ix", ix,
					"And col", col,
				)
			}

			if math.Abs(li.EvalOne(breaks[ix], col)-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany EvalOne should be exact on gridpoints. Not for ix", ix,
					"And col", col,
				)
			}
		}
	}

	// Off grid is close
	newPoints := make([]float64, 150)
	floats.Span(newPoints, breaks[0], breaks[li.n-1])
	for ix := 0; ix < len(newPoints); ix++ {
		have := li.Eval(newPoints[ix])
		for col := 0; col < li.ncol; col++ {
			want[col] = truths[col](newPoints[ix])
			if math.Abs(have[col]-want[col]) > 1e-2 {
				t.Error(
					"LinInterpMany.Eval should be close on gridpoints. Expected", want[col],
					"Found", have[col],
					"At point", newPoints[ix],
				)
			}

			if math.Abs(li.EvalOne(newPoints[ix], col)-want[col]) > 1e-2 {
				t.Error(
					"LinInterpMany.EvalOne should be close on gridpoints. Expected", want[col],
					"Found", have[col],
					"At point", newPoints[ix],
				)
			}
		}
	}
	// Below grid gives first val
	mat.Row(want, 0, li.vals)
	for _, x := range []float64{-100000.0, -4, -3 - 1e-12} {
		have := li.Eval(x)
		for col := 0; col < li.ncol; col++ {
			if math.Abs(have[col]-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany.Eval should equal lower bound below breaks. ",
					"Input", x,
					"Wanted", want[col],
					"Found", have[col],
				)
			}
			if math.Abs(li.EvalOne(x, col)-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany.EvalOne should equal lower bound below breaks. ",
					"Input", x,
					"Wanted", want[col],
					"Found", have[col],
				)
			}
		}
	}

	// Above grid gives first val
	mat.Row(want, li.n-1, li.vals)
	for _, x := range []float64{100000.0, 4, 3 + 1e-15} {
		have := li.Eval(x)
		for col := 0; col < li.ncol; col++ {
			if math.Abs(have[col]-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany.Eval should equal upper bound above breaks. ",
					"Column", col,
					"Input", x,
					"Wanted", want[col],
					"Found", have[col],
				)
			}

			if math.Abs(li.EvalOne(x, col)-want[col]) > 1e-15 {
				t.Error(
					"LinInterpMany.EvalOne should equal upper bound above breaks. ",
					"Column", col,
					"Input", x,
					"Wanted", want[col],
					"Found", have[col],
				)
			}
		}
	}
}

func TestLinInterpMany(t *testing.T) {
	// Uniform interpolation
	breaks := make([]float64, 100)
	floats.Span(breaks, -3, 3)
	vals := mat.NewDense(100, 3, nil)
	for ix := 0; ix < len(breaks); ix++ {
		vals.Set(ix, 0, math.Exp(breaks[ix]))
		vals.Set(ix, 1, math.Sin(breaks[ix]))
		vals.Set(ix, 2, math.Cos(breaks[ix]))
	}

	testLinInterpManyEval(t, breaks, vals, [](func(float64) float64){math.Exp, math.Sin, math.Cos})

	// Non-uniform interpolation
	breaks2 := make([]float64, 40)
	vals2 := mat.NewDense(40, 2, nil)
	breaks2[0] = 0.01
	vals2.Set(0, 0, math.Sin(breaks2[0]))
	vals2.Set(0, 1, math.Cos(breaks2[0]))
	for ix := 1; ix < len(breaks2); ix++ {
		breaks2[ix] = breaks2[ix-1] + rand.Float64()*0.1
		vals2.Set(ix, 0, math.Sin(breaks2[ix]))
		vals2.Set(ix, 1, math.Cos(breaks2[ix]))
	}

	testLinInterpManyEval(t, breaks2, vals2, [](func(float64) float64){math.Sin, math.Cos})

	// make sure we get errors when we expect them
	_, err := NewLinInterpMany(breaks, vals2)
	if err == nil {
		t.Error("Expected NewLinInterp to fail because breaks and vals are unequal length")
	}

	_, err = NewLinInterpMany([]float64{0.1, 0.2, 0.15, 0.3}, mat.NewDense(4, 1, []float64{0.1, 0.2, 0.3, 0.4}))
	if err == nil {
		t.Error("Expected NewLinInterp to fail because breaks is unsorted")
	}
}

func createInterpMany(N int, truths [](func(float64) float64), lb, ub float64) *LinInterpMany {
	ncol := len(truths)
	breaks := make([]float64, N)
	vals := mat.NewDense(N, ncol, nil)
	floats.Span(breaks, lb, ub)
	for ix := 0; ix < N; ix++ {
		for col := 0; col < ncol; col++ {
			vals.Set(ix, col, truths[col](breaks[ix]))
		}
	}
	li, err := NewLinInterpMany(breaks, vals)
	if err != nil {
		panic("Somethow failed to make a LinInterpMany...")
	}
	return li
}

func TestLinInterpManyEvalOnePanicOutOfRange(t *testing.T) {
	li := createInterpMany(5, [](func(float64) float64){math.Cos, math.Sin}, -1, 1)
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()

	li.EvalOne(0.1, 2)
}

func benchmarkLinInterpManyEval(li *LinInterpMany, at float64, b *testing.B) {
	for ix := 0; ix < b.N; ix++ {
		li.Eval(at)
	}
}

func BenchmarkLinInterpManyEvalSinCos5(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(5, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
func BenchmarkLinInterpManyEvalSinCos10(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(10, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
func BenchmarkLinInterpManyEvalSinCos25(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(25, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
func BenchmarkLinInterpManyEvalSinCos50(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(50, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
func BenchmarkLinInterpManyEvalSinCos125(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(125, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
func BenchmarkLinInterpManyEvalSinCos300(b *testing.B) {
	benchmarkLinInterpManyEval(createInterpMany(300, [](func(float64) float64){math.Cos, math.Sin}, -1, 1), 0.1, b)
}
