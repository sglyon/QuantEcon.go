package quantecon

import "testing"

func TestDRVConstructors(t *testing.T) {
	// This is legit
	_, err := NewDiscreteRVRaw([]float64{0.1, 0.2, 0.3, 0.4})
	if err != nil {
		t.Error("Expected success, got an error", err)
	}

	// This is not (row sum > 1)
	_, err = NewDiscreteRVRaw([]float64{0.1, 0.2, 0.3, 0.8})
	if err == nil {
		t.Errorf("Expected 'not sum 1 1 error', was successful")
	}

	// This is not (negative numberin first position)
	_, err = NewDiscreteRVRaw([]float64{-0.1, 0.2, 0.2, 0.3, 0.4})
	if err == nil {
		t.Errorf("Expected 'negative number', was successful")
	}

	// This is not (negative number in later position)
	_, err = NewDiscreteRVRaw([]float64{0.1, -0.2, 0.3, 0.3, 0.4})
	if err == nil {
		t.Errorf("Expected 'negative number', was successful")
	}

	// Random
	for n := 1; n < 10; n++ {
		drv := RandomDiscreteRV(n)
		if len(drv.Q) != n {
			t.Errorf("Expected %d elements from RandomDiscreteRV, found %d", n, len(drv.Q))
		}
	}
}

func TestDRVRand(t *testing.T) {
	drv, _ := NewDiscreteRVRaw([]float64{0.1, 0.2, 0.3, 0.4})
	for _i := 0; _i < 100; _i++ {
		x := drv.Rand()
		if x < 0 || x > 3 {
			t.Errorf("Expected drv sample between 0 and 3, found %d", x)
		}
	}

	xs := drv.Rands(100)
	for _, x := range xs {
		if x < 0 || x > 3 {
			t.Errorf("Expected drv sample between 0 and 3, found %d", x)
		}
	}

	// degenerate distribution
	drv, _ = NewDiscreteRVRaw([]float64{0.0, 0.0, 1.0, 0.0})
	xs = drv.Rands(100)
	for _, x := range xs {
		if x != 2 {
			t.Errorf("Expected drv sample to be 2, found %d", x)
		}
	}

	// only subset of point have mass distribution
	drv, _ = NewDiscreteRVRaw([]float64{0.0, 0.5, 0.5, 0.0})
	xs = drv.Rands(100)
	for _, x := range xs {
		if x != 2 && x != 1 {
			t.Errorf("Expected drv sample to be 1 or 2, found %d", x)
		}
	}
}

func benchmarkDRVRand(drv *DiscreteRV, b *testing.B) {
	for ix := 0; ix < b.N; ix++ {
		drv.Rand()
	}
}

func BenchmarkDRVRand5(b *testing.B)  { benchmarkDRVRand(RandomDiscreteRV(5), b) }
func BenchmarkDRVRand10(b *testing.B) { benchmarkDRVRand(RandomDiscreteRV(10), b) }
func BenchmarkDRVRand15(b *testing.B) { benchmarkDRVRand(RandomDiscreteRV(15), b) }
func BenchmarkDRVRand20(b *testing.B) { benchmarkDRVRand(RandomDiscreteRV(20), b) }
func BenchmarkDRVRand25(b *testing.B) { benchmarkDRVRand(RandomDiscreteRV(25), b) }
func BenchmarkDRVRand27(b *testing.B) { benchmarkDRVRand(RandomDiscreteRV(27), b) }
