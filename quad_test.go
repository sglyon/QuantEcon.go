package quantecon

import (
	"math"
	"testing"
)

type testCase struct {
	n              int
	x1, x2         float64
	nodes, weights []float64
}

func TestQNWGamma(t *testing.T) {
	want := []testCase{
		testCase{
			7, 5.0, 3.0,
			[]float64{4.64166, 10.0317, 17.3161, 26.8299, 39.1152, 55.2208, 77.8447},
			[]float64{0.0769162, 0.385564, 0.399915, 0.125122, 0.0121802, 0.000301303, 9.51709e-7},
		}, testCase{
			7, 1.0, 3.5,
			[]float64{0.675653, 3.59333, 8.98757, 17.1512, 28.6375, 44.5696, 67.885},
			[]float64{0.409319, 0.421831, 0.147126, 0.0206335, 0.00107401, 1.58655e-5, 3.17032e-8},
		},
	}

	for ix, tc := range want {
		n, w, err := Qnwgamma(tc.n, tc.x1, tc.x2)
		if err != nil {
			t.Error("Error when getting qnw: ", err.Error())
		}
		for i := 0; i < tc.n; i++ {
			if math.Abs(n[i]-tc.nodes[i]) > 1e-4 {
				t.Error(
					"Test Case", ix,
					"index", i,
					"Expected node", tc.nodes[i],
					"Found node", n[i],
				)
			}

			if math.Abs(w[i]-tc.weights[i]) > 1e-4 {
				t.Error(
					"Test Case", ix,
					"index", i,
					"Expected weight", tc.weights[i],
					"Found weight", w[i],
				)
			}
		}
	}

	// Error handling
	_, _, err := Qnwgamma(10, -1, 10)
	if err == nil {
		t.Error("Expected an error because `a` was positive, didn't get one")
	}

	_, _, err = Qnwgamma(10, 10, -1)
	if err == nil {
		t.Error("Expected an error because `b` was positive, didn't get one")
	}

	_, _, err = Qnwgamma(10, 0, 0.0)
	if err == nil {
		t.Error("Expected Qnwgamma to not converge when shape parameter is 0")
	}
}
