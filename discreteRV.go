package quantecon

import (
	"math"
	"math/rand"
	"sort"
)

// DiscreteRV represents the distribution of a discrete random variable. The
// sampling weights are contained in q
type DiscreteRV struct {
	q, Q []float64
}

// NewDiscreteRVRaw constructs an instance of `DiscreteRV` using the pdf values
// contained in `q`. Checks are made to ensure that `q` is a valid probability
// density (sums to 1 and is all non-negative)
func NewDiscreteRVRaw(q []float64) (*DiscreteRV, error) {
	out := new(DiscreteRV)
	// make sure first entry is positive
	if q[0] < 0 {
		return out, &valueNegative{0, q[0]}
	}

	n := len(q)
	Q := make([]float64, n)
	Q[0] = q[0]

	// Construct cumsum and ensure all entries of q are positive
	for ix := 1; ix < n; ix++ {
		if q[ix] < 0 {
			return out, &valueNegative{ix, q[ix]}
		}
		Q[ix] = Q[ix-1] + q[ix]
	}

	if math.Abs(Q[n-1]-1) > 1e-14 {
		return out, &rowsNotSum1{0, Q[n-1]}
	}

	out.q = q
	out.Q = Q
	return out, nil
}

// RandomDiscreteRV construct an instance of DiscreteRV with a pseudo-random
// distribution over `n` elements
func RandomDiscreteRV(n int) *DiscreteRV {
	q := make([]float64, n)
	tot := 0.0
	for ix := 0; ix < n; ix++ {
		val := rand.Float64()
		q[ix] = val
		tot += val
	}
	for ix := 0; ix < n; ix++ {
		q[ix] /= tot
	}

	drv, _ := NewDiscreteRVRaw(q)
	return drv
}

// Rand generates one sample from the distribution implied by the discrete
// random variable `drv`
func (drv *DiscreteRV) Rand() int {
	return sort.SearchFloat64s(drv.Q, rand.Float64())
}

// Rands generates `n` samples from the distribution implied by the discrete
// random variable `drv`
func (drv *DiscreteRV) Rands(n int) []int {
	out := make([]int, n)
	for i := 0; i < n; i++ {
		out[i] = drv.Rand()
	}
	return out
}
