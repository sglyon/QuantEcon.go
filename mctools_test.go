package quantecon

import (
	"math"
	"reflect"
	"testing"

	"gonum.org/v1/gonum/mat"
)

type Problem struct {
	n       int
	in, out []float64
}

func kmrMatrixSequential(n int, p float64, epsilon float64) *mat.Dense {
	P := mat.NewDense(n+1, n+1, nil)
	P.Set(0, 0, 1-epsilon*0.5)
	P.Set(0, 1, epsilon*0.5)

	fn := float64(n)

	for ix := 1; ix < n; ix++ {
		n1 := 0.0
		fix := float64(ix)
		if (fix-1)/(fn-1) < p {
			n1++
		}
		if (fix-1)/(fn-1) == p {
			n1 += 0.5
		}

		n2 := 0.0
		if fix/(fn-1) > p {
			n2++
		}
		if fix/(fn-1) == p {
			n2 += 0.5
		}

		P.Set(ix, ix-1, fix/fn*(epsilon*0.5+(1-epsilon)*n1))
		P.Set(ix, ix+1, (fn-fix)/fn*(epsilon*0.5+(1-epsilon)*n2))
		P.Set(ix, ix, 1-P.At(ix, ix-1)-P.At(ix, ix+1))
	}
	P.Set(n, n-1, epsilon*0.5)
	P.Set(n, n, 1-epsilon*0.5)
	return P
}

func kmrMC(n int, p float64, epsilon float64) *MarkovChain {
	mc, err := NewMCDefaultState(kmrMatrixSequential(n, p, epsilon))
	if err != nil {
		panic(err)
	}
	return mc
}

func TestGthSolve(t *testing.T) {
	probs := make([]Problem, 3)
	probs[0] = Problem{2, []float64{0.4, 0.6, 0.2, 0.8}, []float64{0.25, 0.75}}
	probs[1] = Problem{2, []float64{0.0, 1.0, 1.0, 0.0}, []float64{0.5, 0.5}}
	probs[2] = Problem{4, kmrMatrixSequential(3, 1.0/3.0, 1e-14).RawMatrix().Data, []float64{0, 0, 0, 1}}

	for pN, prob := range probs {
		// Construct the MarkovChain
		P := mat.NewDense(prob.n, prob.n, prob.in)
		mc, err := NewMCDefaultState(P)
		if err != nil {
			panic(err)
		}

		// Compute Stationary distribution
		have := mc.GthSolve()

		// Check answer
		for ix := 0; ix < prob.n; ix++ {
			if math.Abs(have[ix]-prob.out[ix]) > 1e-12 {
				t.Error(
					"problem index", pN,
					"Row", ix,
					"Wanted", prob.out[ix],
					"Found", have[ix],
				)
			}
		}
	}
}

func benchmarkGthSolve(mc *MarkovChain, b *testing.B) {
	for ix := 0; ix < b.N; ix++ {
		mc.GthSolve()
	}
}

func BenchmarkGth5(b *testing.B)  { benchmarkGthSolve(kmrMC(5, 1.0/3.0, 1e-3), b) }
func BenchmarkGth10(b *testing.B) { benchmarkGthSolve(kmrMC(10, 1.0/3.0, 1e-3), b) }
func BenchmarkGth15(b *testing.B) { benchmarkGthSolve(kmrMC(15, 1.0/3.0, 1e-3), b) }
func BenchmarkGth20(b *testing.B) { benchmarkGthSolve(kmrMC(20, 1.0/3.0, 1e-3), b) }
func BenchmarkGth25(b *testing.B) { benchmarkGthSolve(kmrMC(25, 1.0/3.0, 1e-3), b) }
func BenchmarkGth27(b *testing.B) { benchmarkGthSolve(kmrMC(27, 1.0/3.0, 1e-3), b) }

func TestRowsSumOne(t *testing.T) {
	mats := [](*mat.Dense){
		mat.NewDense(2, 2, []float64{0.3, 0.6, 0.2, 0.8}),
		mat.NewDense(2, 2, []float64{1e-12, 0, 0.2, 0.8}),
		mat.NewDense(2, 2, []float64{1e-13, 0, 0.2, 0.8}),
		mat.NewDense(2, 2, []float64{9e-13, 0, 0.2, 0.8}),
	}

	for ix, P := range mats {
		_, err := NewMCDefaultState(P)
		if err == nil {
			t.Error(
				"Expected 'not sum to 1' error in index ", ix,
			)
		}
	}
}

func TestNonNegativeP(t *testing.T) {
	mats := [](*mat.Dense){
		mat.NewDense(2, 2, []float64{-0.3, 1.3, 0.2, 0.8}),
		mat.NewDense(2, 2, []float64{1, -1e-15, 0.2, 0.8}),
		mat.NewDense(2, 2, []float64{9e-13, 0, -0.2, 1.2}),
	}

	for ix, P := range mats {
		_, err := NewMCDefaultState(P)
		if err == nil {
			t.Error(
				"Expected 'non-negative entry' error in index ", ix,
			)
		}
	}
}

func getMC(x *MarkovChain, err error) *MarkovChain {
	return x
}

func TestConstructors(t *testing.T) {
	pRaw := []float64{0.3, 0.7, 0.2, 0.8}
	P := mat.NewDense(2, 2, pRaw)
	vals := []float64{1, 2}

	mcs := [](*MarkovChain){
		getMC(NewMarkovChain(P, vals)),
		getMC(NewMCDefaultState(P)),
		getMC(NewMCDefaultStateRaw(pRaw, 2)),
	}

	for ix, mc := range mcs {
		for row := 0; row < 2; row++ {
			val := math.Abs(vals[row] - mc.Values[row])
			if val > 1e-14 {
				t.Error(
					"Default values off by ", val,
					"At row", row,
					"Test case", ix,
				)
			}
		}
	}

}

func TestRouwehhorst(t *testing.T) {
	ρ := 0.2
	σ := 0.4
	μ := 0.0
	for N := 3; N < 26; N++ {
		mc := Rouwenhorst(N, ρ, σ, μ)
		tot := mat.Sum(mat.NewDense(1, N, mc.Values))
		if math.Abs(tot) > 1e-8 {
			t.Error(
				"Rouwenhorst not centered at mu. Expected", 0.0,
				"Found", tot,
			)
		}
		m, n := mc.P.Dims()
		if m != N {
			if math.Abs(tot) > 1e-8 {
				t.Error(
					"Rouwenhorst mc expected rows:", N,
					"Found", m,
				)
			}
		}
		if n != N {
			if math.Abs(tot) > 1e-8 {
				t.Error(
					"Rouwenhorst mc expected cols:", N,
					"Found", n,
				)
			}
		}
		for row := 0; row < N; row++ {
			for col := 0; col < N; col++ {
				if mc.P.At(row, col) < 0 {
					t.Error(
						"Rouwhenhorst found negative value", mc.P.At(row, col),
						"At row", row,
						"At col", col,
					)
				}
			}

		}
	}

	// Case that I know...
	sigma := 0.5
	rho := 0.8
	ybar := 1.0
	mc := Rouwenhorst(3, rho, sigma, ybar)
	wantP := mat.NewDense(3, 3, []float64{0.81, 0.18, 0.01, 0.09, 0.82, 0.09, 0.01, 0.18, 0.81})
	sigmaY := math.Sqrt(sigma * sigma / (1 - rho*rho))
	psi := sigmaY * math.Sqrt(2.0)
	wantX := []float64{-psi + 5.0, 5., psi + 5.0}

	for row := 0; row < 3; row++ {
		for col := 0; col < 3; col++ {
			val := math.Abs(wantP.At(row, col) - mc.P.At(row, col))
			if val > 1e-12 {
				t.Error(
					"Rouwenhorst known case error", val,
					"At row", row,
					"At col", col,
				)
			}
		}

		valX := math.Abs(wantX[row] - mc.Values[row])
		if valX > 1e-12 {
			t.Error(
				"Rouwenhorst known case Values error", valX,
				"At row", row,
			)
		}
	}

}

func TestRowNotSum1Error(t *testing.T) {
	err := rowsNotSum1{1, 10.0}
	want := "Column 1 sums to 1.000000e+01"
	if err.Error() != want {
		t.Error(
			"Wrong error message for interpError",
			"Expected", want,
			"Found", err.Error(),
		)
	}
}

func TestValueNegativeError(t *testing.T) {
	err := valueNegative{1, -10.0}
	want := "Column 1 sums to -1.000000e+01"
	if err.Error() != want {
		t.Error(
			"Wrong error message for interpError",
			"Expected", want,
			"Found", err.Error(),
		)
	}
}

func TestMultipleRecurringPickFirst(t *testing.T) {
	mc, _ := NewMCDefaultStateRaw([]float64{1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1}, 4)
	have := mc.GthSolve()
	want := []float64{1, 0, 0, 0}
	if !reflect.DeepEqual(have, want) {
		t.Error("Expected to see [1 0 0 0], but found", have)
	}
}
