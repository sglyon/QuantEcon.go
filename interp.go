package quantecon

import (
	"errors"
	"sort"

	"gonum.org/v1/gonum/mat"
)

// LinInterp implements piecewise linear interpolation of potentially unevenly spaced data
type LinInterp struct {
	breaks, vals []float64
	n            int
}

// NewLinInterp constructs a piecewise linear interpolator given a sequence of
// break points and the corresponding values
func NewLinInterp(breaks, vals []float64) (*LinInterp, error) {
	out := new(LinInterp)
	if len(breaks) != len(vals) {
		return out, errors.New("breaks and vals must have the same number of elements")
	}

	if !sort.Float64sAreSorted(breaks) {
		return out, errors.New("breaks must be sorted")
	}

	out.breaks = breaks
	out.vals = vals
	out.n = len(breaks)

	return out, nil
}

// Eval evaluates the linear interpolator at a point x
func (li *LinInterp) Eval(x float64) float64 {
	ix := sort.SearchFloat64s(li.breaks, x)

	// handle corner cases
	if ix == 0 {
		return li.vals[0]
	}
	if ix == li.n {
		return li.vals[li.n-1]
	}

	// now do real work. Note, ix is the index of the _upper_ bound on the cell containing x
	z := (li.breaks[ix] - x) / (li.breaks[ix] - li.breaks[ix-1])
	return (1-z)*li.vals[ix] + z*li.vals[ix-1]
}

// LinInterp implements piecewise linear interpolation of potentially unevenly spaced data
type LinInterpMany struct {
	breaks  []float64
	vals    *mat.Dense
	n, ncol int
}

// NewLinInterpMany constructs a piecewise linear interpolator given a sequence of
// break points and the corresponding values. Each column of the values is
// treated as a distinct function
func NewLinInterpMany(breaks []float64, vals *mat.Dense) (*LinInterpMany, error) {
	out := new(LinInterpMany)
	n, ncol := vals.Dims()
	if len(breaks) != n {
		return out, errors.New("length of breaks must match number of columns in vals")
	}

	if !sort.Float64sAreSorted(breaks) {
		return out, errors.New("breaks must be sorted")
	}

	out.n = n
	out.ncol = ncol

	out.breaks = breaks
	out.vals = vals

	return out, nil
}

// Eval evaluates the linear interpolator at a point x
func (li *LinInterpMany) Eval(x float64) []float64 {
	ix := sort.SearchFloat64s(li.breaks, x)

	out := make([]float64, li.ncol)

	// handle corner cases
	if ix == 0 {
		mat.Row(out, 0, li.vals)
		return out
	}
	if ix == li.n {
		mat.Row(out, li.n-1, li.vals)
		return out
	}

	// now do real work. Note, ix is the index of the _upper_ bound on the cell containing x
	z := (li.breaks[ix] - x) / (li.breaks[ix] - li.breaks[ix-1])

	for ind := 0; ind < li.ncol; ind++ {
		out[ind] = (1-z)*li.vals.At(ix, ind) + z*li.vals.At(ix-1, ind)
	}
	return out
}

// EvalOne evaluates the linear interpolator at a point x
func (li *LinInterpMany) EvalOne(x float64, col int) float64 {
	ix := sort.SearchFloat64s(li.breaks, x)

	if col < 0 || col >= li.ncol {
		panic("column out of range!")
	}

	// handle corner cases
	if ix == 0 {
		return li.vals.At(0, col)
	}
	if ix == li.n {
		return li.vals.At(li.n-1, col)
	}

	// now do real work. Note, ix is the index of the _upper_ bound on the cell containing x
	z := (li.breaks[ix] - x) / (li.breaks[ix] - li.breaks[ix-1])
	return (1-z)*li.vals.At(ix, col) + z*li.vals.At(ix-1, col)
}
