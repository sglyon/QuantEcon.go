package quantecon

import (
	"fmt"
	"math"

	"gonum.org/v1/gonum/floats"
	"gonum.org/v1/gonum/mat"
)

type rowsNotSum1 struct {
	Row int
	Val float64
}

func (e *rowsNotSum1) Error() string {
	return fmt.Sprintf("Column %v sums to %e", e.Row, e.Val)
}

type valueNegative struct {
	Row int
	Val float64
}

func (e *valueNegative) Error() string {
	return fmt.Sprintf("Column %v sums to %e", e.Row, e.Val)
}

// MarkovChain is a type for working with fininte, discrete-state Markov chains
type MarkovChain struct {
	P      *mat.Dense // Transition matrix
	Values []float64  // Value of state
}

// NewMarkovChain constructs a new Markv chain with transition matrix `P` and
// state values `Values`. The matrix `P` is checked to ensure that all entries
// are non-negative and that all rows sum to 1
func NewMarkovChain(P *mat.Dense, Values []float64) (*MarkovChain, error) {
	_, n := P.Dims()
	out := new(MarkovChain)
	for row := 0; row < n; row++ {
		rowSum := mat.Sum(P.RowView(row))
		if math.Abs(rowSum-1) > 1e-14 {
			return out, &rowsNotSum1{row, rowSum}
		}

		for col := 0; col < n; col++ {
			if P.At(row, col) < 0 {
				return out, &valueNegative{row, P.At(row, col)}
			}
		}
	}
	out.P = P
	out.Values = Values
	return out, nil
}

// NewMCDefaultState constructs MarkovChain with transition matrix P and Values
// equal to 1..N, with N being the number of rows in P
func NewMCDefaultState(P *mat.Dense) (*MarkovChain, error) {
	_, n := P.Dims()
	Values := make([]float64, n)
	floats.Span(Values, 1, float64(n))
	return NewMarkovChain(P, Values)
}

// NewMCDefaultStateRaw constructs an `n` by `n` MarkovChain with transition
// matrix contained int the slice `data` and  and Values equal to 1..n
func NewMCDefaultStateRaw(data []float64, n int) (*MarkovChain, error) {
	P := mat.NewDense(n, n, data)
	return NewMCDefaultState(P)
}

// GthSolve computes the stationary distribution of an irreducible Markov
// transition matrix (stochastic matrix) or transition rate matrix (generator
// matrix) `A`.
func (mc *MarkovChain) GthSolve() []float64 {
	a := mat.DenseCopyOf(mc.P)
	_, n := a.Dims()
	x := make([]float64, n)

	// reduction
	for k := 0; k < n-1; k++ {
		scale := mat.Sum(a.Slice(k, k+1, k+1, n))
		if scale <= 0 {
			// There is one and only one recurrent class contained in {0, ..., k}
			n = k + 1
			break
		}

		for i := k + 1; i < n; i++ {
			a.Set(i, k, a.At(i, k)/scale)

			for j := k + 1; j < n; j++ {
				a.Set(i, j, a.At(i, j)+a.At(i, k)*a.At(k, j))

			}
		}
	}

	// backward substitution
	x[n-1] = 1.0
	for k := n - 2; k > -1; k-- {
		for i := k + 1; i < n; i++ {
			x[k] = x[k] + x[i]*a.At(i, k)
		}
	}

	// normalization
	norm := 0.0
	for i := 0; i < n; i++ {
		norm += x[i]
	}

	for i := 0; i < n; i++ {
		x[i] /= norm
	}
	return x
}

// Rouwenhorst applies Rouwenhorst's method to approximate AR(1) processes of
// the form `y_t = μ + ρ y_{t-1} + ϵ_t`, where `ϵ_t ~ N(0, σ^2)`
func Rouwenhorst(N int, ρ float64, σ float64, μ float64) *MarkovChain {
	σY := σ / math.Sqrt(1-ρ*ρ)
	p := (1 + ρ) / 2
	ψ := math.Sqrt(float64(N)-1) * σY
	m := μ / (1 - ρ)

	Values, P := _rouwenhorst(p, p, m, ψ, N)
	return &MarkovChain{P, Values}
}

func _rouwenhorst(p float64, q float64, m float64, Δ float64, n int) ([]float64, *mat.Dense) {
	if n == 2 {
		P := mat.NewDense(2, 2, []float64{p, 1 - q, 1 - q, q})
		Values := []float64{m - Δ, m + Δ}
		return Values, P
	}
	_, θnm1 := _rouwenhorst(p, q, m, Δ, n-1)
	p1 := mat.NewDense(n, n, nil)
	p2 := mat.NewDense(n, n, nil)
	p3 := mat.NewDense(n, n, nil)
	p4 := mat.NewDense(n, n, nil)
	for row := 0; row < n-1; row++ {
		for col := 0; col < n-1; col++ {
			p1.Set(row, col, p*θnm1.At(row, col))
			p2.Set(row, col+1, (1-p)*θnm1.At(row, col))
			p3.Set(row+1, col+1, q*θnm1.At(row, col))
			p4.Set(row+1, col, (1-q)*θnm1.At(row, col))
		}
	}
	p4.Add(p4, p3)
	p4.Add(p4, p2)
	p4.Add(p4, p1)

	for row := 1; row < n-1; row++ {
		for col := 0; col < n; col++ {
			p4.Set(row, col, p4.At(row, col)/2)
		}
	}

	Values := make([]float64, n)
	floats.Span(Values, m-Δ, m+Δ)

	return Values, p4
}
